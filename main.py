#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
import sys
from src.math_operation import *
from src.change_approximation import ChangeApproximation
from src.cut_off import CutTheFront
from src.cut_off import CutTheRear
from src.general import LoggingManager
from src.general import FileHandler
from src.const_siglent import ConstSiglent
from src.generate_waveform import GenerateWaveform
from src.triak import Triak
from src.triak import TriakDimmingContinuously
from src.convert_csv import ConvertOscilloscopeCsvToSiglentCSV
from src.general import ChangeSignalLength


class Main(FileHandler):
    def __init__(self):
        super(Main, self).__init__()
        self.logger.info("Program started.")

    def main_loop(self, description=False):

        agneda_list = [
            {
                'name': 'GenerateWaveform',
                'description': GenerateWaveform.info,
                'run': GenerateWaveform().generate
            },
            {
                'name': 'ConvertOscilloscopeCsvToSiglentCSV',
                'description': ConvertOscilloscopeCsvToSiglentCSV.info,
                'run': ConvertOscilloscopeCsvToSiglentCSV().menu_handling
            },
            {
                'name': 'change_signal_length',
                'description': ChangeSignalLength.info,
                'run': ChangeSignalLength().menu_handler
            },
            {
                'name': 'cut_the_front',
                'description': CutTheFront.info,
                'run': CutTheFront().menu_handler
            },
            {
                'name': 'cut_the_rear',
                'description': CutTheRear.info,
                'run': CutTheRear().menu_handler
            },
            {
                'name': 'math_adding_signal (1 long)',
                'description': MathAddingSignal.info,
                'run': MathAddingSignal().menu_handler
            },
            {
                'name': 'connect_2_signals (2 long)',
                'description': Connect2Signals.info,
                'run': Connect2Signals().menu_handler
            },
            {
                'name': 'multiple_signal (n long)',
                'description': MultipleSignal.info,
                'run': MultipleSignal().menu_handler
            },
            {
                'name': 'multiple_amplitude (1 long)',
                'description': MultipleAmplitude.info,
                'run': MultipleAmplitude().menu_handler
            },
            {
                'name': 'approximate - change signal to max samples available in SIGLENT - 16384',
                'description': ChangeApproximation.info,
                'run': ChangeApproximation().menu_handler_change_approximation_and_write_to_file
            },
            {
                'name': 'approximate - custom samples',
                'description': ChangeApproximation.info,
                'run': ChangeApproximation().menu_handler_custom_change_approximation_and_write_to_file
            },
            {
                'name': 'triak_generate (50hz)',
                'description': Triak.info,
                'run': Triak().triak_generate
            },
            {
                'name': 'triak_dimming_continuously (50hz)',
                'description': TriakDimmingContinuously.info,
                'run': TriakDimmingContinuously().menu_handler
            }
        ]

        string = "\n"
        for n in range(len(agneda_list)):
            string += "{}) {}\n".format(n, agneda_list[n]['name'])
            if description:
                string += "\tDescription: {}\n".format(agneda_list[n]['description'])

        self.logger.info(string)
        bullet_point = int(input())
        agneda_list[bullet_point]['run']()

        self.logger.info("SUCCESS - In Siglent set High-Z output impedance.")


if __name__ == '__main__':
    debug = True

    if debug:
        Main().main_loop(description=False)
    else:
        try:
            Main().main_loop(description=False)
        except Exception as error:
            LoggingManager("__main__").logger.error(error)

