# README #

Project was copied from private repository to this one so only head version of files are available.  

This project is for extending functionality of Waveform Generator device:
Siglent SDG1025 Function/Arbitrary waveform generator 25Mhz 125Msa/s.
Propably every version of Siglent generator will works with this software but only SDG1025 was checked. 

This project allows to generate special waveform signal which Siglent SDG1025 can not. Results of running this project is file.csv which should be loaded to Siglent via USB stick and launched as Arbitrary waveform from external source.

Below shortly available features:

0) GenerateWaveform

1) ConvertOscilloscopeCsvToSiglentCSV

2) change_signal_length

3) cut_the_front

4) cut_the_rear

5) math_adding_signal (1 long)

6) connect_2_signals (2 long)

7) multiple_signal (n long)

8) multiple_amplitude (1 long)

9) approximate - change signal to max samples available in SIGLENT - 16384

10) approximate - custom samples

11) triak_generate (50hz)

12) triak_dimming_continuously (50hz)


Every logs are written to logs.log file.