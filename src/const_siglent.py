# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#


class ConstSiglent(object):
    class General(object):
        DATA_STARTING_OFFSET = 13
        DATA_STARTING_OFFSET_OSCILLOSCOPE = 2
        OUTPUT_FILE_NAME = "binary_signal.csv"
        HEADER_LENGTH = 5
        MAX_NUMBER_OF_SAMPLES = 16384
        OUTPUT_FILE_NAME_AFTER_CONNECT = "connect_signal.csv"
        OUTPUT_FILE_NAME_AFTER_MATH = "math_signal.csv"
        """
        DEEP_OF_SAMPLE variable determines how many points is between logical 1 or 0 durong generate signal.
        It means also that all accuracy must be divided by this number. It decreases range of waveform but it
        increases details and rise of edge.
        """
        DEEP_OF_SAMPLE = 100
        MIN_OF_LENGTH = 0.0000001
        LOG_FILE_NAME = "logs.log"

    class KeyName(object):
        QUANTITY_OF_SAMPLE = "quantity_of_samples"
        FREQUENCY = "frequency"
        LENGTH_WHOLE_SIGNAL = "length_whole_signal"
        DATA = "data"
