# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
from src.general import FileHandler
from src.const_siglent import ConstSiglent

class ChangeApproximation(FileHandler):
    info = """It changes quantity of samples. Data is approximating to new number of samples."""

    def __init__(self):
        super(ChangeApproximation, self).__init__()

    def approximation(self, list_apr, b):
        """
        Tomek
        'b' means last index in table - result list must have b+1 items.
        The same with 'a' - it means last index in input list. Input list has a+1 items.

        :param list_apr:
        :param b:
        :return:
        """

        b = int(b)

        if len(list_apr) == b:  #if length input list is the same like you excepted, return input list
            return list_apr

        def transform(list_apr, b):
            out = []
            a = len(list_apr) - 1
            for j in range(0, b):
                r = float(a) / float(b) * j
                value = _solve(list_apr, r)
                out.append(value)
            return out

        def _solve(list_apr, r):
            left = int(r)
            right = int(r) + 1

            if left == (len(list_apr) - 1):  # whether last sample?
                return list_apr[left]
            else:
                # value from range [0,1], move in right in context of one sample range
                move = r - left
                ydelta = float(list_apr[right]) - float(list_apr[left])
                value = ydelta * move + list_apr[left]
                return value

        return transform(list_apr, b)

    def change_approximation_and_write_to_file(self, path_to_csv_file, new_number_of_sample):
        if int(new_number_of_sample) <= 0:
            self.error("ERROR - new_number_of_sample can not be 0 and less during change approximation")
        dict1 = self.read_generic_csv_file(path_to_csv_file, )
        new_list = self.approximation(dict1['data'], new_number_of_sample)
        self.write_generic_csv_file(path_to_csv_file, new_list, dict1['frequency'] ** (-1))

    def menu_handler_change_approximation_and_write_to_file(self):
        file1 = self.ask("Type in path to file")
        self.change_approximation_and_write_to_file(file1, ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)

    def menu_handler_custom_change_approximation_and_write_to_file(self):
        file1 = self.ask("Type in path to file")
        samples = self.ask("Type in quantity of samples")
        self.change_approximation_and_write_to_file(file1, samples)

