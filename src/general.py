# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
import logging
from src.const_siglent import ConstSiglent


class LoggingManager(object):
    # TODO: write python stack error to logger output file
    def __init__(self, class_name):
        # create logger with 'spam_application'
        self.logger = logging.getLogger(class_name)
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(ConstSiglent.General.LOG_FILE_NAME)
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s|%(name)s|%(levelname)s|%(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def logger(self):
        return self.logger

    #TODO: ciekawy przypadek
    @property
    def logger_name(self):
        return self.__class__.__name__


class FileHandler(LoggingManager):
    def __init__(self):
        super(FileHandler, self).__init__(self.logger_name)

    def error(self, string):
        self.logger.info(string)
        self.logger.info("Exiting from program")
        raise AssertionError(string)

    def ask(self, string):
        self.logger.info(string)
        keybord = input()
        self.logger.info("Input data: {}".format(str(keybord)))
        return keybord

    def read_generic_csv_file(self, path_to_csv_file):
        returned_pck = {}
        list_of_data = []

        self.general_check_csv(path_to_csv_file)

        with open(path_to_csv_file) as f1:
            lines = f1.readlines()

            returned_pck[ConstSiglent.KeyName.QUANTITY_OF_SAMPLE] = float(lines[0].split(',')[1])
            returned_pck[ConstSiglent.KeyName.FREQUENCY] = float(lines[1].split(',')[1])
            returned_pck[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL] = float(returned_pck[ConstSiglent.KeyName.FREQUENCY] ** (-1))

            for n in range(ConstSiglent.General.DATA_STARTING_OFFSET, len(lines)):
                list_of_data.append(float(lines[n].split(',')[1]))

            returned_pck[ConstSiglent.KeyName.DATA] = list_of_data

        return returned_pck

    def write_generic_csv_file(self, path_to_csv_file, raw_data, whole_signal_length):

        with open(path_to_csv_file, 'w') as f1:
            f1.write("data lenth,{}\r\n".format(len(raw_data)))
            f1.write("frequency,{}\r\n".format(float(whole_signal_length ** (-1))))
            f1.write("amp,20.000000000\r\n")
            f1.write("offset,0.000000000\r\n")
            f1.write("phase,0.000000000\r\n")
            for n in range(ConstSiglent.General.HEADER_LENGTH, ConstSiglent.General.DATA_STARTING_OFFSET - 1):
                f1.write("\r\n")
            f1.write("xpos,value\r\n")

            for n in range(len(raw_data)):
                f1.write("{:.6e},{}\r\n".format(n * float(whole_signal_length) /
                                                float(len(raw_data)), str(raw_data[n])))

            f1.close()
            self.logger.info("New file has been writen as '{}'".format(path_to_csv_file))

    def general_check_csv(self, path_to_csv_file):
        if self.check_that_csv_is_from_oscilloscope(path_to_csv_file):
            ConvertOscilloscopeCsvToSiglentCSV().convert_oscilloscope_to_easywave(path_to_csv_file)
        if not self.check_that_csv_is_from_easywave_program(path_to_csv_file):
            self.error("Error - Format csv file must be like EasyWave program generates.")

    def check_that_csv_is_from_oscilloscope(self, path_to_oscilloscope_file):
        with open(path_to_oscilloscope_file) as f1:
            lines = f1.readlines()

            # check that first oscilloscope header exist
            if len(lines) < 2:
                self.error("ERROR - Empty file.")

            # check oscilloscope header
            if lines[1].split(',')[0] != "Second" or lines[1].split(',')[1] != "Volt":
                return False

            n = None
            for n in range(ConstSiglent.General.DATA_STARTING_OFFSET_OSCILLOSCOPE, len(lines)):
                try:  # check all data that are a numbers
                    float(lines[n].split(',')[0])
                    float(lines[n].split(',')[1])
                except ValueError:
                    self.error("ERROR - There is no data in data section in csv file or one sample is not a number. "
                               "Bad data: {},{}".format(lines[n].split(',')[0], lines[n].split(',')[1]))

        return True

    def check_that_csv_is_from_easywave_program(self, path_to_oscilloscope_file):
        with open(path_to_oscilloscope_file) as f1:
            lines = f1.readlines()
            try:
                lines[1]
            except IndexError:
                self.error("ERROR - probably empty file.")

            if lines[0].split(',')[0] != "data lenth" or \
                            lines[1].split(',')[0] != "frequency" or \
                            lines[2].split(',')[0] != "amp" or \
                            lines[3].split(',')[0] != "offset" or \
                            lines[4].split(',')[0] != "phase" or \
                            lines[ConstSiglent.General.DATA_STARTING_OFFSET - 1].split(',')[0] != "xpos":
                self.error("ERROR - Header wrong. Checking csv EasyWave_program file failed.")

        n = None
        try:  # check all data that is a number
            for n in range(ConstSiglent.General.DATA_STARTING_OFFSET, len(lines)):
                float(lines[n].split(',')[0])
                float(lines[n].split(',')[1])
        except ValueError:
            self.error("ERROR - There is no data in data section in csv file or one sample is not a number. "
                       "Bad data: {},{}".format(lines[n].split(',')[0], lines[n].split(',')[1]))

        try:  # check all header value is a number
            for n in range(ConstSiglent.General.HEADER_LENGTH):
                float(lines[n].split(',')[1])
        except ValueError:
            self.error("ERROR - one value in header is wrong. Bad value: {},{}"
                       .format(lines[n].split(',')[0], lines[n].split(',')[1]))

        return True


class ChangeSignalLength(FileHandler):
    info = "it only changes length of signal. It means that also frequency has been changed. Data is the same."

    def __init__(self):
        super(ChangeSignalLength, self).__init__()

    def change_signal_length(self, file_name, new_length):
        dict1 = self.read_generic_csv_file(file_name)
        self.write_generic_csv_file(file_name, dict1[ConstSiglent.KeyName.DATA], new_length)

    def menu_handler(self):
        file1 = self.ask("Type in path to file")
        dict1 = self.read_generic_csv_file(file1)
        self.logger.info(
            "Signal length of existing csv file is: {}".format(dict1[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL]))
        new_length = float(self.ask("Type in new length of period."))
        self.change_signal_length(file1, new_length)


