# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
from src.const_siglent import ConstSiglent
from src.change_approximation import ChangeApproximation
from src.general import FileHandler


class Connect2Signals(FileHandler):
    info = """Function connect_2_signals one waveform to another. Order of input parameter is important.
    It depends which waveform will be first. Time of signals is not changed - there is no any cutting.
    Only number of sample will change."""

    def __init__(self):
        super(Connect2Signals, self).__init__()

    def connect_2_signals(self, path_to_csv_file1, path_to_csv_file2):
        """

        :param path_to_csv_file1:
        :param path_to_csv_file2:
        :return:
        """

        self.general_check_csv(path_to_csv_file1)
        self.general_check_csv(path_to_csv_file2)

        ChangeApproximation().change_approximation_and_write_to_file(path_to_csv_file1,
                                                                     ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)
        ChangeApproximation().change_approximation_and_write_to_file(path_to_csv_file2,
                                                                     ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)

        dict1 = self.read_generic_csv_file(path_to_csv_file1)
        dict2 = self.read_generic_csv_file(path_to_csv_file2)
        length_2signals = dict1[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL] + dict2[
            ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL]

        # check which waveform is longer and shorter, first signal is longer then second
        if dict1[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL] > dict2[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL]:
            longer_dict = dict1
            shorter_dict = dict2
            path_to_csv_file_shorter = path_to_csv_file2
        else:
            shorter_dict = dict1
            longer_dict = dict2
            path_to_csv_file_shorter = path_to_csv_file1

        time_sample_long = longer_dict[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL] / longer_dict[
            ConstSiglent.KeyName.QUANTITY_OF_SAMPLE]
        need_quantity_sample_short = shorter_dict[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL] / time_sample_long
        if int(need_quantity_sample_short) <= 0:
            self.error("ERROR - length difference of two signals is too big.")
        ChangeApproximation().change_approximation_and_write_to_file(path_to_csv_file_shorter,
                                                                     need_quantity_sample_short)

        # connect_2_signals data section
        dict1 = self.read_generic_csv_file(path_to_csv_file1)
        dict2 = self.read_generic_csv_file(path_to_csv_file2)
        data_sum = dict1['data'] + dict2['data']

        self.write_generic_csv_file(ConstSiglent.General.OUTPUT_FILE_NAME_AFTER_CONNECT, data_sum, length_2signals)
        ChangeApproximation().change_approximation_and_write_to_file(
            ConstSiglent.General.OUTPUT_FILE_NAME_AFTER_CONNECT,
            ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)

    def menu_handler(self):
        file1 = self.ask("Type in path to first file")
        file2 = self.ask("Type in path to second file")
        self.connect_2_signals(file1, file2)


class MultipleSignal(FileHandler):
    info = """it simply multiple self waveform. Waveform is connecting at the end of waveform etc."""

    def __init__(self):
        super(MultipleSignal, self).__init__()

    def multiple_signal(self, path_to_csv_file, multiple):
        """

        :param path_to_csv_file:
        :param multiple:
        :return:
        """
        multiple = int(multiple)

        if multiple <= 0:
            self.error("ERROR - during multiple_signal multiple must be greater then 0.")
        file_read = self.read_generic_csv_file(path_to_csv_file)
        raw_data = list(file_read['data'])
        new_data = raw_data * multiple
        length = file_read['length_whole_signal']
        self.write_generic_csv_file(path_to_csv_file, new_data, float(length * multiple))
        ChangeApproximation().change_approximation_and_write_to_file(path_to_csv_file,
                                                                     ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)

    def menu_handler(self):
        file1 = self.ask("Type in path to file")
        multiple = self.ask("Type in multiple")
        self.multiple_signal(file1, multiple)


class MathAddingSignal(FileHandler):
    info = """It does math operand '+' on each sample from two waveform. Example. Two signals: square and noise.
    After perform this function result will be: noisy square wave."""

    def __init__(self):
        super(MathAddingSignal, self).__init__()

    def math_adding_signal(self, path_to_csv_file1, path_to_csv_file2, file_name=ConstSiglent.General.OUTPUT_FILE_NAME_AFTER_MATH):
        """

        :param path_to_csv_file1:
        :param path_to_csv_file2:
        :return:
        """

        math_data = []

        self.general_check_csv(path_to_csv_file1)
        self.general_check_csv(path_to_csv_file2)

        dict1 = self.read_generic_csv_file(path_to_csv_file1)
        dict2 = self.read_generic_csv_file(path_to_csv_file2)

        # check which waveform is shorter, first signal is shorter then second
        if dict1[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL] < dict2[ConstSiglent.KeyName.LENGTH_WHOLE_SIGNAL]:
            shorter_dict = dict1
            longer_dict = dict2
        else:
            longer_dict = dict1
            shorter_dict = dict2

        # alignment length shorter signal to longer by connect
        multiply = longer_dict['length_whole_signal'] / shorter_dict['length_whole_signal']
        tmp = list(list(shorter_dict['data']) * int(multiply))
        diff = len(longer_dict['data']) - len(tmp)
        for n in range(diff):
            tmp.append(shorter_dict['data'][n])

        tmp = ChangeApproximation().approximation(tmp, longer_dict['quantity_of_samples'])

        length_longer_data = len(longer_dict['data'])
        # do math, core of this method
        for k in range(length_longer_data):
            math_data.append(longer_dict['data'][k] + tmp[k])

        # voltage should be in range: max -10,  10
        for k in range(length_longer_data):
            if math_data[k] > 10:
                math_data[k] = 10
            if math_data[k] < -10:
                math_data[k] = -10

        self.write_generic_csv_file(file_name, math_data,
                                    longer_dict['length_whole_signal'])

    def menu_handler(self):
        file1 = self.ask("Type in path to first file")
        file2 = self.ask("Type in path to second file")
        self.math_adding_signal(file1, file2)


class MultipleAmplitude(FileHandler):
    info = """This method allows user to change amplitude - value of voltage for each sample. For divider greater
    then 1 amplitude will gain. For divider between 1 and 0 amplitude will decrease."""

    def __init__(self):
        super(MultipleAmplitude, self).__init__()

    def multiple_amplitude(self, path_to_csv_file, multiplier):
        """
        :param path_to_csv_file:
        :param multiplier:
        :return:
        """
        multiplier = float(multiplier)

        if multiplier < 0:
            self.error("ERROR - Divider during divide_amplitude can not be less then zero.")
        self.general_check_csv(path_to_csv_file)
        dict1 = self.read_generic_csv_file(path_to_csv_file)

        for n in range(len(dict1['data'])):
            new_value = float(dict1['data'][n]) * float(multiplier)
            if new_value > 10:
                new_value = 10
            elif new_value < -10:
                new_value = -10
            dict1['data'][n] = float(new_value)

        self.write_generic_csv_file(path_to_csv_file, dict1['data'], dict1['length_whole_signal'])

    def menu_handler(self):
        file1 = self.ask("Type in path to file")
        multiplier = self.ask("Type in multiplier of amplitude:")
        self.multiple_amplitude(file1, multiplier)

