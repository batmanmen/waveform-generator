# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
from src.change_approximation import ChangeApproximation
from src.general import FileHandler
from src.const_siglent import ConstSiglent


class GenerateWaveform(FileHandler):
    info = """This class can generate file with custom digital signal (like this: 101010) for SIGLENT SDG1000 Series
    Function/Arbitrary Waveform Generator. User can:
    - set amplitude. Whole waveform will have the same amplitude
    - all signals have the same width of each bit.
    - user can determine width of bit. The same width is used for logical 1 and 0
    - user can determine content of waveform (1010001010100101)
    - there is no possible to add some different signals between waveform
    - order of bit send is hardcoded in program. One flag manage it: reverse=True
    - generated file can be also use in EasyWave.exe program. It is added together with SIGLENT SDG1000
    - generated file need to be moved to usb pendrive and loaded in SIGLENT SDG1000"""

    def __init__(self):
        super(GenerateWaveform, self).__init__()

    def _check_waveform(self, binary_waveform):
        # it checks that waveform contain only 1 and 0 chars. If doesn't, it is not binary chain.
        for char in binary_waveform:
            if char != '0' and char != '1':
                self.error("ERROR - binary_waveform has invalid char. Only 0 and 1 is valid.")

    def _user_interface(self):
        # Amplitude
        amplitude = self.ask("Type in amplitude in [V] of waveform. It must be greater then 0V and less then 10V (e.g. 3.3):")
        if not -10 < float(amplitude) < 10:
            self.error("ERROR - amplitude must be between 0<x<10V")

        # length of one bit
        length_one_bit = self.ask("Type in length of one bit in [s]. Min is: {:.6e}. Format e.g. 0.0000015. If you want to have more, " \
                                  "change 'deep_of_sample' variable in program." \
                                  .format(ConstSiglent.General.MIN_OF_LENGTH * ConstSiglent.General.DEEP_OF_SAMPLE))
        if float(length_one_bit) < (ConstSiglent.General.MIN_OF_LENGTH * ConstSiglent.General.DEEP_OF_SAMPLE):
            self.error("ERROR - length of one bit can not be less then {}"
                       .format(ConstSiglent.General.MIN_OF_LENGTH * ConstSiglent.General.DEEP_OF_SAMPLE))
        self.logger.info("{}s ({}hz)".format(length_one_bit, (float(length_one_bit)) ** -1))

        # voltage
        binary_waveform = self.ask("Type in binary string which will be equivalent of waveform (e.g. 1001). You can use {} bits. Change " \
                         "'deep_of_sample' variable to increase it at the expense of lower accuracy." \
                         .format(ConstSiglent.General.MAX_NUMBER_OF_SAMPLES / ConstSiglent.General.DEEP_OF_SAMPLE))
        self._check_waveform(binary_waveform)
        if (len(str(binary_waveform)) * ConstSiglent.General.DEEP_OF_SAMPLE) > ConstSiglent.General.MAX_NUMBER_OF_SAMPLES:
            self.error("ERROR - there is too much of samples - {} but should be: {}. Quantity of logical bits: {}," \
                       " details: x{}. You can decrease in program 'deep_of_sample' variable." \
                       .format((len(str(binary_waveform)) * ConstSiglent.General.DEEP_OF_SAMPLE), ConstSiglent.General.MAX_NUMBER_OF_SAMPLES,
                               len(str(binary_waveform)), ConstSiglent.General.DEEP_OF_SAMPLE))

        return amplitude, length_one_bit, binary_waveform

    def _create_raw_data(self, amplitude, binary_waveform):
        list_data = []

        for n in sorted(range(len(str(binary_waveform))), reverse=False):  # to reverse order use False flag
            state = binary_waveform[n]
            for k in range(ConstSiglent.General.DEEP_OF_SAMPLE):
                list_data.append(int(state) * float(amplitude))

        return list_data

    def generate(self):
        amplitude, length_one_bit, binary_waveform = self._user_interface()
        list_data = self._create_raw_data(amplitude, binary_waveform)
        whole_length = len(str(binary_waveform)) * float(length_one_bit)
        self.write_generic_csv_file(ConstSiglent.General.OUTPUT_FILE_NAME, list_data, whole_length)
        ChangeApproximation().change_approximation_and_write_to_file(ConstSiglent.General.OUTPUT_FILE_NAME,
                                                                     ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)

