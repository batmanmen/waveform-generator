# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
from src.general import FileHandler
import math
from src.const_siglent import ConstSiglent
from src.change_approximation import ChangeApproximation


class Triak(FileHandler):
    info = """It creates cutted sine in exactly the same way how it does triak. It can be used simulating
    dimming 230V lights"""

    def _generate_sine_raw_data(self, amplitude):
        default_sample = int(ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)
        raw_data = []

        for n in range(default_sample):
            raw_data.append(float(amplitude) * float(math.sin(n * (2 * math.pi / default_sample))))

        return raw_data

    def _triak_user_interface(self):  #collect information from user
        # fill_percentage
        fill_percentage = self.ask("Type in fill_percentage in [%]. 0<=x<=100 (e.g. 33.5):")
        if not 0 <= float(fill_percentage) <= 100:
            self.error("ERROR - fill_percentage must be between 0<=x<=10V")

        # Amplitude
        amplitude = self.ask("Type in amplitude in [V] of waveform. It must be greater then 0V and less then 10V (e.g. 3.3):")
        if not -10 < float(amplitude) < 10:
            self.error("ERROR - amplitude must be between -10<x<10V")

        return fill_percentage, amplitude

    def _triak_adjust_wave(self, fill_percentage, amplitude):
        fill_percentage = float(fill_percentage)
        amplitude = float(amplitude)

        if not 0 <= fill_percentage <= 100:
            self.error("ERROR - wrong percentage")
        fill_percentage /= 100

        new_list = []
        raw_sine_data = self._generate_sine_raw_data(amplitude)
        half_length_list = int(len(raw_sine_data) / 2)
        for n in range(half_length_list):
            if n < (fill_percentage * half_length_list):
                new_list.append(raw_sine_data[n])
            else:
                new_list.append(float(0))

        for k in range(half_length_list, len(raw_sine_data)):
            if k < half_length_list + (fill_percentage * half_length_list):
                new_list.append(raw_sine_data[k])
            else:
                new_list.append(float(0))

        return new_list

    def triak_generate(self):
        fill_percentage, amplitude = self._triak_user_interface()
        triak_list = self._triak_adjust_wave(fill_percentage, amplitude)
        self.write_generic_csv_file(ConstSiglent.General.OUTPUT_FILE_NAME, triak_list, float(50 ** (-1)))


class TriakDimmingContinuously(Triak):
    # TODO: if you change step (variable _triak_dimming_continuously->step) from 20 to another value, frequency of result file is no 50hz
    # FIXME: it doesn't work properly. Squere sine signal.
    info = """It dimming for example light from min to max and revers."""

    def _triak_dimming_continuously(self, tempo, amplitude):
        list1 = []
        step = 20
        multiplier = int((float(tempo) / (float(step + 1) * 2)) / (50 ** (-1)))

        #dimming 0% -> 100%
        for i in range(step+1):
            for z in range(multiplier):
                list1 += self._triak_adjust_wave(i * (100/step), amplitude)

        #dimming 100% -> 0%
        for k in range(step, -1, -1):
            for z in range(multiplier):
                list1 += self._triak_adjust_wave(k * (100/step), amplitude)

        whole_time = float((20+2) * 2 * float(multiplier) * (50**(-1)))
        return list1, whole_time

    def menu_handler(self):
        #tempo = self.ask("Type in tempo of dimming in second. 0% -> 100% -> 0%")
        tempo = 15
        amplitude = self.ask("Type in amplitude in [V]. Note default time for 0% -> 100% -> 0% is 15s")
        self.logger.info("wait...")
        list1, whole_time = self._triak_dimming_continuously(tempo, amplitude)
        self.logger.info("wait...")
        self.write_generic_csv_file(ConstSiglent.General.OUTPUT_FILE_NAME, list1, float(whole_time))
        self.logger.info("wait...")
        ChangeApproximation().change_approximation_and_write_to_file(ConstSiglent.General.OUTPUT_FILE_NAME,
                                                             ConstSiglent.General.MAX_NUMBER_OF_SAMPLES)
