# -*- coding: utf-8 -*-
#
#   Destination hardware: Siglent Function/Arbitrary waveform generator 25Mhz 125Msa/s
#   hardware oscilloscope Rigol ds1052E 50Mhz 1Gsa/s
#   software: EasyWave.exe delivered together with generator Siglent
#
#   Author: Dariusz Rzedowski dariusz.rzedowski@gmail.com,
#
#   Created: 20/08/2016
#
from src.general import FileHandler


class CutOff(FileHandler):
    def __init__(self):
        super(CutOff, self).__init__()

    def _cut(self, path_to_csv_file, rear=True):
        cutted_data = []

        self.general_check_csv(path_to_csv_file)

        dict1 = self.read_generic_csv_file(path_to_csv_file)
        frequency = dict1['frequency']
        old_length = float(frequency ** (-1))
        old_sample_quantity = dict1['quantity_of_samples']

        self.logger.info("Your signal has length: {}[s]. You can only reduce this time. What time your new whole signal should " \
              "have? (in [s] e.g. 0.0023)".format(old_length))
        new_length = float(input())
        self.logger.info("Input data: {}".format(new_length))
        if new_length > old_length or new_length < 0:
            self.error("ERROR - new length greater then old one or less then 0.")

        divider = old_length / new_length
        new_sample_quantity = old_sample_quantity / divider

        if rear:  # cutting data from the end of waveform. It means that after cutting last data will be lost
            for n in range(int(new_sample_quantity)):
                cutted_data.append(dict1['data'][n])
        else:  # cutting from front. It means that after cutting first data will lost
            for n in range(len(dict1['data']) - int(new_sample_quantity), len(dict1['data'])):
                cutted_data.append(dict1['data'][n])

        self.write_generic_csv_file(path_to_csv_file, cutted_data, new_length)


class CutTheFront(CutOff):
    info = """It cutoffs waveform to determined by user new length. New waveform can be only shorter then original.
    Example: Whole waveform has length: 0.03200000[s]. User can cutoff it to for example 0.001000[s].
    Rest of data is lost. This cut waveform from the front."""

    def __init__(self):
        super(CutTheFront, self).__init__()

    def cut_the_front(self, path_to_csv_file):
        self._cut(path_to_csv_file, rear=False)

    def menu_handler(self):
        file1 = self.ask("Type in path to file")
        self.cut_the_front(file1)


class CutTheRear(CutOff):
    info = """It cutoffs waveform to determined by user new length. New waveform can be only shorter then original.
    Example: Whole waveform has length: 0.03200000[s]. User can cutoff it to for example 0.001000[s].
    Rest of data is lost. This cut waveform from the rear."""

    def __init__(self):
        super(CutTheRear, self).__init__()

    def cut_the_rear(self, path_to_csv_file):
        self._cut(path_to_csv_file, rear=True)

    def menu_handler(self):
        file1 = self.ask("Type in path to file")
        self.cut_the_rear(file1)
